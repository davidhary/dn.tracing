# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.1.9083] - 2024-11-13 Preview 202304
* Update packages.
* Increment version to 9083.

## [1.1.8956] - 2024-07-09 Preview 202304
* Set projects to target .NET Standard 2.0 alone.
* Update .NET standard 2.0 enhanced methods and attributes.

## [1.1.8949] - 2024-07-02 Preview 202304
* Update to .Net 8.
* Implement MS Test SDK project format.
* Update documentation of standard classes.
* Update libraries versions.
* Apply code analysis rules.
* Use ordinal instead of ordinal ignore case string comparisons.
* Apply constants style.
* Generate assembly attributes.

## [1.1.8525] - 2023-04-05 Preview 202304
* Split README.MD to attribution, cloning, open-source and read me files.
* Add code of conduct, contribution and security documents.
* Increment version.

## [1.0.8503] - 2023-04-13 Preview 202304
* Add creative common license. Implement nullable. change namespace to cc.isr.Tracing. 

## [1.0.8125] - 2022-03-31
* Pass tests in project reference mode. 

## [1.0.8110] - 2022-03-16
* Use the ?. operator, without making a copy of the delegate, 
to check if a delegate is non-null and invoke it in a thread-safe way.

## [1.0.8103] - 2022-03-09
* Forked from [dn.core].

&copy; 2012 Integrated Scientific Resources, Inc. All rights reserved.

[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.1.9083]: https://bitbucket.org/davidhary/dn.tracing/src/main/
