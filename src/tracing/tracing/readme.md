# About

[cc.isr.Tracing]  is a .Net library supporting Tracing operations.

# How to Use

# Unit Test Trace Listener
An example for using the trace listener in a test class where 
the test object might be emitting trace events that need to be asserted by test processes.
```
[ClassInitialize()]
public static void TestClassInitialize( TestContext testContext )
{
    try
    {
        string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
        TraceListener = new cc.isr.Tracing.TestWriterQueueTraceListener( name, System.Diagnostics.SourceLevels.Warning );
        _ = System.Diagnostics.Trace.Listeners.Add( TraceListener );

    }
    catch
    {
        // cleanup to meet strong guarantees
        try
        {
            TestClassCleanup();
        }
        finally
        {
        }

        throw;
    }
}

[ClassCleanup()]
public static void TestClassCleanup()
{
    System.Diagnostics.Trace.Listeners.Remove( TraceListener );
    TraceListener.Dispose();
}

[TestInitialize()]
public void MyTestInitialize()
{
    TraceListener.ClearQueue();
}

[TestCleanup()]
public void MyTestCleanup()
{
    if ( !TraceListener.Queue.IsEmpty )
        Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{String.Join( Environment.NewLine, TraceListener.Queue.ToArray() )}" );
}

private static cc.isr.Tracing.TestWriterQueueTraceListener TraceListener { get; set; }
```

# Key Features

* Provides classes for tracing and notification.

# Main Types

The main types provided by this library are:

* _AsyncTextWriterTraceListener_ An asynchronous text writer trace listener. This class cannot be inherited.
* _AsyncTraceEventWriterTraceListener_ An asynchronous text writer trace listener. This class cannot be inherited.
* _TextWriterQueueTraceListener_ A concurrent text writer queue trace listener.
* _TraceEventMessage_ TraceEventMessage
* _TraceEventQueueTraceListener_ A concurrent trace event queue trace listener.
* _TracingPlatform_ Provides the tracing platform.

# Feedback

[cc.isr.Tracing] is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Tracing Repository].

[cc.isr.Tracing]: https://bitbucket.org/davidhary/dn.tracing

