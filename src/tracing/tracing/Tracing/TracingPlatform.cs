
namespace cc.isr.Tracing;

/// <summary>   Provides the tracing platform. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
public class TracingPlatform : IDisposable
{
    #region " construction and cleanup "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    private TracingPlatform()
    { }

    #region " disposable support "

    /// <summary> Gets the disposed indicator. </summary>
    /// <value> The disposed indicator. </value>
    public bool IsDisposed { get; private set; }

    /// <summary>
    /// Releases the unmanaged resources used by the class.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    /// <param name="disposing"> True to release both managed and unmanaged resources; false to
    /// release only unmanaged resources. </param>
    protected virtual void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        try
        {
            if ( disposing )
            {
                this.RemoveTraceListeners();
                if ( this._lazyTextWriterTraceListener.IsValueCreated )
                    this.TextWriterTraceListener?.Dispose();
                if ( this._lazyTraceEventWriterTraceListener.IsValueCreated )
                    this.TraceEventWriterTraceListener?.Dispose();
                this._criticalTraceEventTraceListener?.Dispose();
                this._textWriterQueueTraceListener?.Dispose();
            }
        }
        finally
        {
            this.IsDisposed = true;
        }
    }

    /// <summary> Calls <see cref="Dispose(bool)" /> to cleanup. </summary>
    /// <remarks>
    /// Do not make this method Overridable (virtual) because a derived class should not be able to
    /// override this method.
    /// </remarks>
    public void Dispose()
    {
        this.Dispose( true );
        // Take this object off the finalization(Queue) and prevent finalization code 
        // from executing a second time.
        GC.SuppressFinalize( this );
    }

    /// <summary>
    /// This destructor will run only if the Dispose method does not get called. It gives the base
    /// class the opportunity to finalize. Do not provide destructors in types derived from this
    /// class.
    /// </summary>
    /// <remarks> David, 2020-09-17. </remarks>
    ~TracingPlatform()
    {
        // Do not re-create Dispose clean-up code here.
        // Calling Dispose(false) is optimal for readability and maintainability.
        this.Dispose( false );
    }

    #endregion

    #region " lazy singleton "

    /// <summary> Creates a new default instance of this class. </summary>
    public static void NewInstance()
    {
        lock ( _syncLocker )
        {
            _instance = new TracingPlatform();
        }
    }

    /// <summary> The locking object to enforce thread safety when creating the singleton instance. </summary>
    private static readonly object _syncLocker = new();

    /// <summary> The singleton instance </summary>
    private static TracingPlatform? _instance;

    /// <summary> Instantiates the default instance of the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved or if is was disposed. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
    public static TracingPlatform Instance
    {
        get
        {
            if ( !Instantiated )
            {
                NewInstance();
            }
            return _instance!;
        }
    }

    /// <summary> Gets True if the singleton instance was instantiated and is not disposed. </summary>
    /// <value> True if the singleton instance was instantiated and is not disposed. </value>
    public static bool Instantiated
    {
        get
        {
            lock ( _syncLocker )
            {
                return _instance is not null && !_instance.IsDisposed;
            }
        }
    }

    #endregion

    #endregion

    #region " add a trace listener "

    /// <summary>   The trace listeners. </summary>
    private readonly List<WeakReference> _traceListeners = [];

    /// <summary>   Adds a trace listener if one does not already exists. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="traceListener">    The trace listener. </param>
    public void AddTraceListener( System.Diagnostics.TraceListener traceListener )
    {
        if ( !System.Diagnostics.Trace.Listeners.Contains( traceListener ) )
            this._traceListeners.Add( new WeakReference( traceListener ) );
        _ = System.Diagnostics.Trace.Listeners.Add( traceListener );
    }

    /// <summary>   Removes the trace listener described by traceListener. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="traceListener">    The trace listener. </param>
    public void RemoveTraceListener( System.Diagnostics.TraceListener traceListener )
    {
        if ( System.Diagnostics.Trace.Listeners.Contains( traceListener ) )
            _ = this._traceListeners.Remove( new WeakReference( traceListener ) );
        System.Diagnostics.Trace.Listeners.Remove( traceListener );
    }

    /// <summary>   Removes the trace listeners. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    private void RemoveTraceListeners()
    {
        foreach ( WeakReference wr in this._traceListeners )
        {
            this.RemoveTraceListener( ( System.Diagnostics.TraceListener ) wr.Target );
        }
    }

    #endregion

    #region " text writer trace listeners "

    /// <summary>   The lazy Show text writer trace listener constructor. </summary>
    private readonly Lazy<AsyncTextWriterTraceListener> _lazyTextWriterTraceListener = new( () =>
    {
        AsyncTextWriterTraceListener listener = new( nameof( TracingPlatform.TextWriterTraceListener ),
                                                                                  TracingPlatform.Instance.TextWriterSourceLevel );
        // add the listener to the trace
        TracingPlatform.Instance.AddTraceListener( listener );
        return listener;
    } );

    /// <summary>   Gets the asynchronous trace listener for displaying messages. </summary>
    /// <value> The asynchronous trace listener for displaying messages. </value>
    private AsyncTextWriterTraceListener TextWriterTraceListener => this._lazyTextWriterTraceListener.Value;

    private System.Diagnostics.SourceLevels _textWriterSourceLevel = System.Diagnostics.SourceLevels.Information;

    /// <summary>
    /// Gets or sets the <see cref="TextWriterTraceListener"/> Text Writer source level above which
    /// trace messages are ignored.
    /// </summary>
    /// <value> The minimum source level. </value>
    public System.Diagnostics.SourceLevels TextWriterSourceLevel
    {
        get => this._lazyTextWriterTraceListener.IsValueCreated ? this.TextWriterTraceListener.SourceLevel : this._textWriterSourceLevel;
        set
        {
            if ( value != this.TextWriterSourceLevel )
            {
                if ( this._lazyTextWriterTraceListener.IsValueCreated )
                    this.TextWriterTraceListener.ApplyListenerFilter( value );
                this._textWriterSourceLevel = value;
            }
        }
    }

    /// <summary>   Adds a text writer. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The text writer. </param>
    public void AddTextWriter( ITextWriter textWriter )
    {
        this.TextWriterTraceListener.AddTextWriter( textWriter );
    }

    /// <summary>   Removes a text writer described by textWriter. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="textWriter">   The text writer. </param>
    public void RemoveTextWriter( ITextWriter textWriter )
    {
        this.TextWriterTraceListener.RemoveTextWriter( textWriter );
    }

    #endregion

    #region " trace event trace listeners "

    /// <summary>   The lazy Show text writer trace listener constructor. </summary>
    private readonly Lazy<AsyncTraceEventWriterTraceListener> _lazyTraceEventWriterTraceListener = new( () =>
    {
        AsyncTraceEventWriterTraceListener listener = new( nameof( TracingPlatform.TraceEventWriterTraceListener ),
                                                                                  TracingPlatform.Instance.TraceEventWriterSourceLevel );
        // add the listener to the trace
        TracingPlatform.Instance.AddTraceListener( listener );
        return listener;
    } );

    /// <summary>   Gets the asynchronous trace listener for displaying messages. </summary>
    /// <value> The asynchronous trace listener for displaying messages. </value>
    private AsyncTraceEventWriterTraceListener TraceEventWriterTraceListener => this._lazyTraceEventWriterTraceListener.Value;

    private System.Diagnostics.SourceLevels _traceEventWriterSourceLevel = System.Diagnostics.SourceLevels.Information;

    /// <summary>
    /// Gets or sets the TraceEvent writer source level above which trace messages are ignored.
    /// This level filters all trace writers that listen to (that are written into by) this trace listener. 
    /// </summary>
    /// <value> The minimum source level. </value>
    public System.Diagnostics.SourceLevels TraceEventWriterSourceLevel
    {
        get => this._lazyTraceEventWriterTraceListener.IsValueCreated ? this.TraceEventWriterTraceListener.SourceLevel : this._traceEventWriterSourceLevel;
        set
        {
            if ( value != this.TraceEventWriterSourceLevel )
            {
                if ( this._lazyTraceEventWriterTraceListener.IsValueCreated )
                    this.TraceEventWriterTraceListener.ApplyListenerFilter( value );
                this._traceEventWriterSourceLevel = value;
            }
        }
    }

    /// <summary>   Adds a trace event writer. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="writer">   The writer. </param>
    public void AddTraceEventWriter( ITraceEventWriter writer )
    {
        this.TraceEventWriterTraceListener.AddTraceEventWriter( writer );
    }

    /// <summary>   Removes the trace event writer described by writer. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="writer">   The writer. </param>
    public void RemoveTraceEventWriter( ITraceEventWriter writer )
    {
        this.TraceEventWriterTraceListener.RemoveTraceEventWriter( writer );
    }

    #endregion

    #region " trace listeners for critical warnings and errors "

    private TraceEventQueueTraceListener? _criticalTraceEventTraceListener;

    /// <summary>   Gets the platform trace listener for warning and error messages. </summary>
    /// <value> The platform trace listener from warning and error message. </value>
    public TraceEventQueueTraceListener CriticalTraceEventTraceListener
    {
        get
        {
            if ( !this.CriticalTraceEventTraceListenerInstantiated )
            {
                this._criticalTraceEventTraceListener = new TraceEventQueueTraceListener( nameof( TracingPlatform.CriticalTraceEventTraceListener ),
                                                                                          TracingPlatform.Instance.CriticalTraceEventSourceLevel );
            }
            return this._criticalTraceEventTraceListener!;
        }
    }

    /// <summary>
    /// Gets a value indicating whether the critical trace event trace listener instantiated.
    /// </summary>
    /// <value> True if critical trace event trace listener instantiated, false if not. </value>
    public bool CriticalTraceEventTraceListenerInstantiated => this._criticalTraceEventTraceListener is not null && this._criticalTraceEventTraceListener.IsDisposed;

    private System.Diagnostics.SourceLevels _criticalTraceEventSourceLevel = System.Diagnostics.SourceLevels.Warning;

    /// <summary>   Gets or sets the CriticalTraceEvent source level above which trace messages are ignored. </summary>
    /// <value> The minimum source level. </value>
    public System.Diagnostics.SourceLevels CriticalTraceEventSourceLevel
    {
        get => this.CriticalTraceEventTraceListenerInstantiated ? this.CriticalTraceEventTraceListener.SourceLevel : this._criticalTraceEventSourceLevel;
        set
        {
            if ( value != this.CriticalTraceEventSourceLevel )
            {
                if ( this.CriticalTraceEventTraceListenerInstantiated )
                    this.CriticalTraceEventTraceListener.ApplyListenerFilter( value );
                this._criticalTraceEventSourceLevel = value;
            }
        }
    }

    #endregion

    #region " concurrent text writer trace listener "

    private TestWriterQueueTraceListener _textWriterQueueTraceListener = default!;

    /// <summary>   Gets the platform concurrent <see cref="TestWriterQueueTraceListener"/>. </summary>
    /// <value> The platform trace listener from text messages . </value>
    public TestWriterQueueTraceListener TextWriterQueueTraceListener
    {
        get
        {
            if ( !this.TextWriterQueueTraceListenerInstantiated )
                this._textWriterQueueTraceListener = new TestWriterQueueTraceListener( nameof( TracingPlatform.TextWriterQueueTraceListener ),
                                                                                       TracingPlatform.Instance.TextWriterQueueSourceLevel );
            return this._textWriterQueueTraceListener;
        }
    }

    /// <summary>
    /// Gets a value indicating whether the text writer queue trace listener instantiated.
    /// </summary>
    /// <value> True if text writer queue trace listener instantiated, false if not. </value>
    public bool TextWriterQueueTraceListenerInstantiated => this._textWriterQueueTraceListener is not null && this._textWriterQueueTraceListener.IsDisposed;


    private System.Diagnostics.SourceLevels _textWriterQueueSourceLevel = System.Diagnostics.SourceLevels.Information;

    /// <summary>   Gets or sets the source level above which text writer queue trace messages are ignored. </summary>
    /// <value> The minimum source level. </value>
    public System.Diagnostics.SourceLevels TextWriterQueueSourceLevel
    {
        get => this.TextWriterQueueTraceListenerInstantiated ? this.TextWriterQueueTraceListener.SourceLevel : this._textWriterQueueSourceLevel;
        set
        {
            if ( value != this.TextWriterQueueSourceLevel )
            {
                if ( this.TextWriterQueueTraceListenerInstantiated )
                    this.TextWriterQueueTraceListener.ApplyListenerFilter( value );
                this._textWriterQueueSourceLevel = value;
            }
        }
    }

    #endregion
}

