using System.Diagnostics;

namespace cc.isr.Tracing;
/// <summary>
/// Interface for trace event writer, which is capable of filtering <see cref="TraceEvent"/> by
/// its <see cref="TraceLevel"/>.
/// </summary>
/// <remarks>   David, 2021-02-09. </remarks>
public interface ITraceEventWriter
{
    /// <summary>   Gets or sets the name of the text writer. </summary>
    /// <value> The name. </value>
    string Name { get; set; }

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    TraceEventType TraceLevel { get; set; }

    /// <summary>   Trace event. </summary>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    void TraceEvent( TraceEventType eventType, string message );
}
/// <summary>   A text writers concurrent dictionary. </summary>
/// <remarks>   David, 2021-02-23. </remarks>
/// <remarks>   Constructor. </remarks>
/// <remarks>   David, 2021-02-23. </remarks>
/// <param name="name"> The name. </param>
public class TraceEventWritersConcurrentDictionary( string name ) : System.Collections.Concurrent.ConcurrentDictionary<string, ITraceEventWriter>(), ITraceEventWriter
{
    /// <summary>   Constructor. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    public TraceEventWritersConcurrentDictionary() : this( System.Guid.NewGuid().ToString() )
    { }

    /// <summary>   Attempts to add. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="traceEventWriter">   The text writer. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public bool TryAdd( ITraceEventWriter traceEventWriter )
    {
        return this.TryAdd( traceEventWriter.Name, traceEventWriter );
    }

    /// <summary>   Attempts to remove. </summary>
    /// <remarks>   David, 2021-02-23. </remarks>
    /// <param name="traceEventWriter">   The text writer. </param>
    /// <returns>   True if it succeeds; otherwise, false. </returns>
    public bool TryRemove( ITraceEventWriter traceEventWriter )
    {
        return this.TryRemove( traceEventWriter.Name, out _ );
    }

    /// <summary>   Gets the name of the text writer. </summary>
    /// <value> The name. </value>
    public string Name { get; set; } = name;

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    public TraceEventType TraceLevel { get; set; }

    /// <summary>
    /// When overridden in a derived class, writes the specified message and trace event type to the listener you create
    /// in the derived class.
    /// </summary>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    public void TraceEvent( TraceEventType eventType, string message )
    {
        if ( this.Count > 0 )
            foreach ( ITraceEventWriter traceEventWriter in this.Values )
            {
                traceEventWriter.TraceEvent( eventType, message );
            }
    }

    /// <summary>
    /// When overridden in a derived class, writes the specified message and trace event type to the listener you create
    /// in the derived class.
    /// </summary>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    void ITraceEventWriter.TraceEvent( TraceEventType eventType, string message )
    {
        this.TraceEvent( eventType, message );
    }
}
