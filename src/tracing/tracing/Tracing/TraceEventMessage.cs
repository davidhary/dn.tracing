using System.Diagnostics;

namespace cc.isr.Tracing;

/// <summary>   A trace event message. </summary>
/// <remarks>   David, 2021-03-05. </remarks>
/// <remarks>   Constructor. </remarks>
/// <remarks>   David, 2021-03-05. </remarks>
/// <param name="eventType">    Type of the event. </param>
/// <param name="message">      The message. </param>
public struct TraceEventMessage( TraceEventType eventType, string message )
{
    /// <summary>   Gets or sets the type of the event. </summary>
    /// <value> The type of the event. </value>
    public TraceEventType EventType { get; set; } = eventType;

    /// <summary>   Gets or sets the message. </summary>
    /// <value> The message. </value>
    public string Message { get; set; } = message;
}
