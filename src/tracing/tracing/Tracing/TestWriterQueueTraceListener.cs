using System.Collections.Concurrent;
using System.Diagnostics;

namespace cc.isr.Tracing;

/// <summary>   A concurrent text writer queue trace listener. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
public class TestWriterQueueTraceListener : TraceListener, ITextWriter
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="name">             The <see cref="TraceListener"/> name. </param>
    /// <param name="sourceLevel">      (Optional) Source level. </param>
    /// <param name="maximumLength">    (Optional) The maximum length of the Queue. 0 if infinite. </param>
    /// <param name="ignoreHeader">     (Optional) True if ignore header, false if not. </param>
    public TestWriterQueueTraceListener( string name,
        SourceLevels sourceLevel = SourceLevels.Information,
        int maximumLength = 1000,
        bool ignoreHeader = true ) : base( name )
    {
        this.Queue = new ConcurrentQueue<string>();
        this.MaximumLength = maximumLength;
        this.IgnoreHeader = ignoreHeader;
        this.InitialSourceLevel = sourceLevel;
        this.SourceLevel = sourceLevel;
        // this.Filter = new cc.isr.Tracing.EventTypeFilter( sourceLevel );
        this.Filter = new System.Diagnostics.EventTypeFilter( sourceLevel );
    }

    /// <summary>   Gets or sets a value indicating whether this object is disposed. </summary>
    /// <value> True if this object is disposed, false if not. </value>
    internal bool IsDisposed { get; private set; }

    /// <summary>
    /// Releases the unmanaged resources used by the
    /// <see cref="TraceListener"></see> and optionally releases the managed
    /// resources.
    /// </summary>
    /// <remarks>   David, 2021-08-28. </remarks>
    /// <param name="disposing">    true to release both managed and unmanaged resources; false to
    ///                             release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        this.IsDisposed = true;
        base.Dispose( disposing );
    }

    #endregion

    #region " filtering "

    /// <summary>   Gets or sets the initial trace listener source level. </summary>
    /// <value> The initial trace listener source level. </value>
    public SourceLevels InitialSourceLevel { get; private set; }

    /// <summary>   Gets or sets the trace listener source level. </summary>
    /// <value> The trace listener source level. </value>
    public SourceLevels SourceLevel { get; private set; }

    /// <summary>
    /// Applies the trace listener filter described by <paramref name="sourceLevel"/>.
    /// </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    /// <param name="sourceLevel">  Source level. </param>
    public void ApplyListenerFilter( SourceLevels sourceLevel )
    {
        this.SourceLevel = sourceLevel;
        this.Filter = new System.Diagnostics.EventTypeFilter( this.SourceLevel );
    }

    /// <summary>   Restore the initial trace listener filter. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    public void RestoreListenerFilter()
    {
        this.ApplyListenerFilter( this.InitialSourceLevel );
    }

    #endregion

    #region " ignore header "

    /// <summary>   Gets or sets a value indicating whether to ignore the source name and level header. </summary>
    /// <value> True if ignore header, false if not. </value>
    public bool IgnoreHeader { get; set; }

    /// <summary>   Query if 'message' is header record. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    /// <param name="message">  The message. </param>
    /// <returns>   True if header record, false if not. </returns>
    private static bool IsHeaderRecord( string message )
    {
        return message.EndsWith( " : " );
    }

    #endregion

    #region " trace listener implementation "

    /// <summary>   Gets a queue of <see cref="string"/> messages. </summary>
    /// <value> A queue of messages. </value>
    public ConcurrentQueue<string> Queue { get; private set; }

    /// <summary>   Clears the queue. </summary>
    /// <remarks>   David, 2021-04-26. </remarks>
    public void ClearQueue()
    {
        this.Queue.Clear();
    }

    /// <summary>   Gets or sets the maximum length. </summary>
    /// <value> The maximum length of the Queue. 0 if infinite. </value>
    public int MaximumLength { get; set; }

    /// <summary>   Adds an object onto the end of this queue. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  The message. </param>
    private void Enqueue( string message )
    {
        while ( this.MaximumLength > 0 && this.Queue.Count >= this.MaximumLength )
        {
            _ = this.Queue.TryDequeue( out _ );
        }
        this.Queue.Enqueue( message );
    }

    /// <summary>
    /// When overridden in a derived class, writes the specified message to the listener you create
    /// in the derived class.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void Write( string message )
    {
        if ( this.IgnoreHeader && IsHeaderRecord( message ) )
            return;
        // !@# 20211111: 
        // this was removed because the trace listener was emitting  both the write and write line methods.
        // this.Enqueue( message );
    }

    /// <summary>
    /// When overridden in a derived class, writes a message to the listener you create in the
    /// derived class, followed by a line terminator.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void WriteLine( string message )
    {
        if ( this.IgnoreHeader && IsHeaderRecord( message ) )
            return;
        this.Enqueue( message );
    }

    #endregion
}
/// <summary>
/// Summary:
///     Indicates whether a listener should trace based on the event type.
/// </summary>
/// <remarks>   David, 2021-11-11. </remarks>
public class EventTypeFilter : TraceFilter
{
    /// <summary>
    /// Summary:
    ///     Initializes a new instance of the System.Diagnostics.EventTypeFilter class.
    /// 
    /// Parameters:
    ///   level:
    ///     A bitwise combination of the System.Diagnostics.SourceLevels values that specifies the
    ///     event type of the messages to trace.
    /// </summary>
    /// <remarks>   David, 2021-11-11. </remarks>
    /// <param name="level">    The level. </param>
    public EventTypeFilter( SourceLevels level ) => this.EventType = level;

    private int _maximumTraceEventType;
    private SourceLevels _eventType;

    /// <summary>
    /// Summary:
    ///     Gets or sets the event type of the messages to trace.
    /// 
    /// Returns:
    ///     A bitwise combination of the System.Diagnostics.SourceLevels values.
    /// </summary>
    /// <value> The type of the event. </value>
    public SourceLevels EventType
    {
        get => this._eventType;
        set
        {
            this._eventType = value;
            this._maximumTraceEventType = value switch
            {
                SourceLevels.ActivityTracing => ( int ) TraceEventType.Verbose,
                SourceLevels.All => ( int ) TraceEventType.Verbose,
                SourceLevels.Critical => ( int ) TraceEventType.Critical,
                SourceLevels.Error => ( int ) TraceEventType.Error,
                SourceLevels.Information => ( int ) TraceEventType.Information,
                SourceLevels.Off => 0,
                SourceLevels.Verbose => ( int ) TraceEventType.Verbose,
                SourceLevels.Warning => ( int ) TraceEventType.Warning,
                _ => ( int ) TraceEventType.Information,
            };
        }
    }

    /// <summary>
    /// Summary:
    ///     Determines whether the trace listener should trace the event.
    /// </summary>
    /// <remarks>   David, 2021-11-11. </remarks>
    /// <param name="cache">            The <see cref="TraceEventCache"></see>
    ///                                 that contains information for the trace event. </param>
    /// <param name="source">           The name of the source. </param>
    /// <param name="eventType">        One of the
    ///                                 <see cref="TraceEventType"></see> values
    ///                                 specifying the type of event that has caused the trace. </param>
    /// <param name="id">               A trace identifier number. </param>
    /// <param name="formatOrMessage">  Either the format to use for writing an array of arguments
    ///                                 specified by the <paramref name="args"/> parameter, or a message to write. </param>
    /// <param name="args">             An array of argument objects. </param>
    /// <param name="data1">            A trace data object. </param>
    /// <param name="data">             An array of trace data objects. </param>
    /// <returns>   true to trace the specified event; otherwise, false. </returns>
    public override bool ShouldTrace( TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data )
    {
        return ( int ) eventType > this._maximumTraceEventType;
    }
}
