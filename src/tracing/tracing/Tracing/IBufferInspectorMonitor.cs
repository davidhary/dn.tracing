namespace cc.isr.Tracing;
/// <summary>
/// Defines a mechanism for checking on a buffer metadata inspection mechanism.
/// </summary>
public interface IBufferInspectorMonitor
{
    /// <summary>   Invoked by async class to supply the inspector to the monitor. </summary>
    /// <param name="inspector">    buffer inspector. </param>
    void StartMonitoring( IBufferInspector inspector );

    /// <summary>   Invoked by async class to indicate that it is being Disposed. </summary>
    /// <param name="inspector">    buffer inspector. </param>
    void StopMonitoring( IBufferInspector inspector );
}
