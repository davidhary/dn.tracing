namespace cc.isr.Tracing;
/// <summary>
/// Provides a way to inspect the state of a buffer queue.
/// </summary>
public interface IBufferInspector
{
    /// <summary>
    /// Configured maximum number of items permitted to be held in the buffer awaiting ingestion.
    /// </summary>
    ///
    /// <exception cref="ObjectDisposedException"> The inspector consumer has been disposed. </exception>
    /// <value> The size of the buffer. </value>
    int BufferSize { get; }

    /// <summary>   Current moment-in-time Count of items currently awaiting ingestion. </summary>
    ///
    /// <exception cref="ObjectDisposedException"> The inspector consumer has been disposed. </exception>
    /// <value> The count. </value>
    int Count { get; }

    /// <summary>
    /// Accumulated number of messages dropped due to breaches of <see cref="BufferSize"/> limit.
    /// </summary>
    long DroppedMessagesCount { get; }
}
