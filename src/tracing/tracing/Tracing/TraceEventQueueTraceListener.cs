using System.Collections.Concurrent;
using System.Diagnostics;

namespace cc.isr.Tracing;

/// <summary>   A concurrent trace event queue trace listener. </summary>
/// <remarks>   David, 2021-03-05. </remarks>
public class TraceEventQueueTraceListener : TraceListener, ITraceEventWriter
{
    #region " construction "

    /// <summary>   Default constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="name">             The <see cref="TraceListener"/> name. </param>
    /// <param name="sourceLevel">      (Optional) Source level. </param>
    /// <param name="maximumLength">    (Optional) The maximum length of the Queue. 0 if infinite. </param>
    /// <param name="ignoreHeader">     (Optional) True if ignore header, false if not. </param>
    public TraceEventQueueTraceListener( string name,
        SourceLevels sourceLevel = SourceLevels.Information,
        int maximumLength = 1000,
        bool ignoreHeader = true ) : base( name )
    {
        this.Queue = new ConcurrentQueue<TraceEventMessage>();
        this.MaximumLength = maximumLength;
        this.IgnoreHeader = ignoreHeader;
        this.InitialSourceLevel = sourceLevel;
        this.SourceLevel = sourceLevel;
        this.Filter = new System.Diagnostics.EventTypeFilter( sourceLevel );
        this.TraceLevel = ToTraceEventType( sourceLevel );
    }

    /// <summary>   Converts a <see cref="SourceLevel"/> to a trace event type. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="level">    The level. </param>
    /// <returns>   Level as a System.Diagnostics.TraceEventType. </returns>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0072:Add missing cases", Justification = "<Pending>" )]
    public static TraceEventType ToTraceEventType( SourceLevels level )
    {
        return level switch
        {
            SourceLevels.Critical => System.Diagnostics.TraceEventType.Critical,
            SourceLevels.Error => System.Diagnostics.TraceEventType.Error,
            SourceLevels.Information => System.Diagnostics.TraceEventType.Information,
            SourceLevels.Warning => System.Diagnostics.TraceEventType.Warning,
            SourceLevels.Verbose => System.Diagnostics.TraceEventType.Verbose,
            _ => System.Diagnostics.TraceEventType.Information,
        };
    }

    /// <summary>   Gets or sets a value indicating whether this object is disposed. </summary>
    /// <value> True if this object is disposed, false if not. </value>
    internal bool IsDisposed { get; private set; }

    /// <summary>
    /// Releases the unmanaged resources used by the
    /// <see cref="TraceListener"></see> and optionally releases the managed
    /// resources.
    /// </summary>
    /// <remarks>   David, 2021-08-28. </remarks>
    /// <param name="disposing">    true to release both managed and unmanaged resources; false to
    ///                             release only unmanaged resources. </param>
    protected override void Dispose( bool disposing )
    {
        if ( this.IsDisposed ) return;
        this.IsDisposed = true;
        base.Dispose( disposing );
    }

    #endregion

    #region " filtering "

    /// <summary>   Gets or sets the initial trace listener source level. </summary>
    /// <value> The initial trace listener source level. </value>
    public SourceLevels InitialSourceLevel { get; private set; }

    /// <summary>   Gets or sets the trace listener source level. </summary>
    /// <value> The trace listener source level. </value>
    public SourceLevels SourceLevel { get; private set; }

    /// <summary>
    /// Applies the trace listener filter described by <paramref name="sourceLevel"/>.
    /// </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    /// <param name="sourceLevel">  Source level. </param>
    public void ApplyListenerFilter( SourceLevels sourceLevel )
    {
        this.TraceLevel = ToTraceEventType( sourceLevel );
        this.SourceLevel = sourceLevel;
        this.Filter = new System.Diagnostics.EventTypeFilter( this.SourceLevel );
    }

    /// <summary>   Restore the initial trace listener filter. </summary>
    /// <remarks>   David, 2021-02-09. </remarks>
    public void RestoreListenerFilter()
    {
        this.ApplyListenerFilter( this.InitialSourceLevel );
    }

    #endregion

    #region " ignore header "

    /// <summary>   Gets or sets a value indicating whether to ignore the source name and level header. </summary>
    /// <value> True if ignore header, false if not. </value>
    public bool IgnoreHeader { get; set; }

    /// <summary>   Query if 'message' is header record. </summary>
    /// <remarks>   David, 2021-02-16. </remarks>
    /// <param name="message">  The message. </param>
    /// <returns>   True if header record, false if not. </returns>
    private static bool IsHeaderRecord( string message )
    {
        return message.EndsWith( " : " );
    }

    #endregion

    #region " trace listener implementation "

    /// <summary>   Gets a queue of <see cref="TraceEventMessage"/> messages. </summary>
    /// <value> A queue of messages. </value>
    public ConcurrentQueue<TraceEventMessage> Queue { get; private set; }

    /// <summary>   Gets or sets the maximum length. </summary>
    /// <value> The maximum length of the Queue. 0 if infinite. </value>
    public int MaximumLength { get; set; }

    /// <summary>   Adds an object onto the end of this queue. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    private void Enqueue( TraceEventType eventType, string message )
    {
        while ( this.MaximumLength > 0 && this.Queue.Count >= this.MaximumLength )
        {
            _ = this.Queue.TryDequeue( out _ );
        }
        if ( eventType <= this.TraceLevel )
            this.Queue.Enqueue( new TraceEventMessage( eventType, message ) );
    }

    /// <summary>
    /// When overridden in a derived class, writes the specified message to the listener you create
    /// in the derived class.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void Write( string message )
    { }

    /// <summary>
    /// When overridden in a derived class, writes a message to the listener you create in the
    /// derived class, followed by a line terminator.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void WriteLine( string message )
    { }

    /// <summary>
    /// Writes trace information, a message, and event information to the listener specific output.
    /// </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="eventCache">   A <see cref="TraceEventCache" /> object that
    ///                             contains the current process ID, thread ID, and stack trace
    ///                             information. </param>
    /// <param name="source">       A name used to identify the output, typically the name of the
    ///                             application that generated the trace event. </param>
    /// <param name="eventType">    One of the <see cref="TraceEventType" />
    ///                             values specifying the type of event that has caused the trace. </param>
    /// <param name="id">           A numeric identifier for the event. </param>
    /// <param name="message">      A message to write. </param>
    public override void TraceEvent( TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message )
    {
        this.TraceEvent( eventType, message );
    }

    /// <summary>   Dequeue all messages. </summary>
    /// <remarks>   David, 2021-03-06. </remarks>
    /// <returns>   A <see cref="string" />. </returns>
    public string DequeueAllMessages()
    {
        System.Text.StringBuilder builder = new();
        foreach ( TraceEventMessage traceEventMessage in this.Queue )
        {
            _ = builder.AppendLine( traceEventMessage.Message );
        };
        return builder.ToString().Replace( "\r\n   --- End of ", ".\r\n   --- End of " ).TrimEnd( "\r\n".ToCharArray() );
    }

    #endregion

    #region " trace event writer implementation "

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    public TraceEventType TraceLevel { get; set; }

    /// <summary>   Trace event. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    public void TraceEvent( TraceEventType eventType, string message )
    {
        if ( this.IgnoreHeader && IsHeaderRecord( message ) )
            return;
        this.Enqueue( eventType, message );
    }

    #endregion
}
