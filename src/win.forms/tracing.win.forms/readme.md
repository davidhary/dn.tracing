# About

cc.isr.Tracing.WinForms is a .Net library supporting the display of tracing information or events.

# How to Use

## Message Box Trace Listener

An example for using the trace listener to display trace messages.

```
using cc.isr.Tracing.WinForms;

private TextBox _TraceMessagesBox = new();
private cc.isr.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }

/// <summary>   Initializes the trace listener. </summary>
/// <remarks>   David, 2021-03-04. </remarks>
private void InitializeTraceListener()
{
    this.TextBoxTextWriter = new( this._TraceMessagesBox );
    this.TextBoxTextWriter.ContainerPanel = this._MessagesTabPage;
    this.TextBoxTextWriter.TabCaption = "Log";
    this.TextBoxTextWriter.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( new byte[] { 240 } );
    this.TextBoxTextWriter.ResetCount = 1000;
    this.TextBoxTextWriter.PresetCount = 500;

    this.AddDisplayTextWriter( this.TextBoxTextWriter );

}
```

## Message Box Trace Listener and Trace Alert Container on a Windows Form

```
public partial class Dashboard : Form
{

    private TextBox _TraceMessagesBox = new();
    private System.Windows.Forms.ToolStripTextBox _AlertsToolStripTextBox = new();
    private cc.isr.Tracing.WinForms.TextBoxTraceEventWriter TextBoxTextWriter { get; set; }
    private cc.isr.Tracing.WinForms.TraceAlertContainer WarningButtonTraceAlertContainer { get; set; }

    /// <summary> Default constructor. </summary>
    public Dashboard() : base()
    {
        this.InitializeComponent();

        this._AlertsToolStripTextBox.Visible = false;

        // set the text box text writer
        this.TextBoxTextWriter = new( this._TraceMessagesBox );
        this.TextBoxTextWriter.ContainerTreeNode = null;
        this.TextBoxTextWriter.HandleCreatedCheckEnabled = false;
        this.TextBoxTextWriter.TabCaption = "Log";
        this.TextBoxTextWriter.CaptionFormat = "{0} " + Convert.ToChar( 0x1C2 );
        this.TextBoxTextWriter.ResetCount = 1000;
        this.TextBoxTextWriter.PresetCount = 500;
        this.TextBoxTextWriter.TraceLevel = AppSettings.Instance.Settings.MessageDisplayLevel;
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

        this.WarningButtonTraceAlertContainer = new( this._AlertsToolStripTextBox.TextBox, this._TraceMessagesBox );
        this.WarningButtonTraceAlertContainer.AlertAnnunciatorText = "Alert";
        this.WarningButtonTraceAlertContainer.AlertLevel = TraceEventType.Warning;
        this.WarningButtonTraceAlertContainer.AlertSoundEnabled = true;
        cc.isr.Tracing.TracingPlatform.Instance.AddTraceEventWriter( this.WarningButtonTraceAlertContainer );

        this._AlertsToolStripTextBox.Visible = true;
        this._AlertsToolStripTextBox.TextBox.Visible = false;
    }
}
```

# Key Features

* User interface components that consume trace events.

# Main Types

The main types provided by this library are:

* _TextBoxTraceEventWriter_ A text box _isr.Tracing.ITraceEventWriter_.
* _TextBoxTraceListener_ Implements a _TraceListener_ using a _TextBox_ with _isr.Tracing.ITraceEventWriter_.
* _TextBoxWriterTraceListener_ A text box writer trace listener.
* _TraceAlertContainer_ A base container for a trace event alert Annunciator.

# Feedback

cc.isr.Tracing.WinForms is released as open source under the MIT license.
Bug reports and contributions are welcome at the [Tracing Repository].

[Tracing Repository]: https://bitbucket.org/davidhary/dn.tracing

