using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.Tracing.WinForms;

/// <summary>   A text box <see cref="ITraceEventWriter"/>. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
public class TextBoxTraceEventWriter : ITraceEventWriter, INotifyPropertyChanged
{
    #region " construction "

    private readonly TextBox _target;

    /// <summary>   Constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="target">   Target for the. </param>
    public TextBoxTraceEventWriter( TextBox target )
    {
        this.Name = System.Guid.NewGuid().ToString();
        this._target = target;
        this._lines = [];
        if ( target.Multiline )
            this._lines.AddRange( target.Lines );
        this.ResetCount = 200;
        this.PresetCount = 100;
        this.Appending = false;
        this.TabCaption = "Log";
        this.CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( [240] );
        target.VisibleChanged += this.HandleTargetVisibleChanged;
        if ( target.GetContainerControl()?.ActiveControl is Control ctrl )
            ctrl.VisibleChanged += new EventHandler( this.HandleTargetVisibleChanged );
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " i trace event writer implementation "

    /// <summary>   Gets or sets the name of the text writer. </summary>
    /// <value> The name. </value>
    public string Name { get; set; }

    /// <summary>
    /// When overridden in a derived class, writes the specified message to the listener you create
    /// in the derived class.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public void Write( string message )
    {
        if ( this._target.InvokeRequired )
            _ = this._target.BeginInvoke( new MethodInvoker( () => this.Write( message ) ) );
        else if ( !this.HandleCreatedCheckEnabled || this._target.IsHandleCreated )
            this.AddMessageInternal( message );
    }

    /// <summary>
    /// When overridden in a derived class, writes a message to the listener you create in the
    /// derived class, followed by a line terminator.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public void WriteLine( string message )
    {
        this.Write( $"{message}{Environment.NewLine}" );
    }

    /// <summary>   Gets or sets the maximum level for tracing an event. </summary>
    /// <remarks> Only events with the same or lower <see cref="TraceEventType"/> are  
    /// allowed (filtered in). </remarks>
    /// <value> The trace level. </value>
    public TraceEventType TraceLevel { get; set; }

    /// <summary>   Trace event. </summary>
    /// <remarks>   David, 2021-03-06. </remarks>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    public void TraceEvent( TraceEventType eventType, string message )
    {
        if ( eventType <= this.TraceLevel )
            if ( this._target.Multiline )
                this.Write( message );
            else
                this.WriteLine( message );
    }

    /// <summary>   Adds a message. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  The message. </param>
    private delegate void AddMessage( string message );

    /// <summary>   Adds a message internal. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    private void AddMessageInternal( string message )
    {
        // No need to lock text box as this function will only 
        // ever be executed from the UI thread
        if ( string.IsNullOrWhiteSpace( message ) )
            return;

        if ( this._target.Multiline )
        {
            if ( this._target.Lines.Length == 0 )
                this._lines.Clear();
            string[] values = message.Split( ["\r\n", "\r", "\n"], StringSplitOptions.None );
            if ( values.Length == 1 )
            {
                if ( this.Appending )
                {
                    this._lines.Add( message );
                }
                else
                {
                    this._lines.Insert( 0, message );
                }
            }
            else if ( this.Appending )
            {
                this._lines.AddRange( values );
            }
            else
            {
                this._lines.InsertRange( 0, values );
            }

            if ( this._lines.Count > this.ResetCount )
            {
                if ( this.Appending )
                {
                    this._lines.RemoveRange( 0, this._lines.Count - this.PresetCount );
                }
                else
                {
                    this._lines.RemoveRange( this.PresetCount, this._lines.Count - this.PresetCount );
                }
            }

            this._target.Lines = [.. this._lines];
        }
        else
        {
            if ( this.Appending )
            {
                this._target.AppendText( message );
            }
            else
            {
                _ = this._target.Text = $"{message}{this._target.Text}";
            }
        }
        if ( !this.Appending )
        {
            this._target.SelectionStart = 0;
            this._target.SelectionLength = 0;
        }

        this.TextLength = this._target.TextLength;
        this.ObservedTextLength = this.UserVisible ? this.TextLength : this.ObservedTextLength;
        this.UpdateCaption();
    }

    #endregion

    #region " text update information "

    private void HandleClearEvent( object? sender, EventArgs e )
    {
        this._lines.Clear();
    }

    /// <summary>   The lines. </summary>
    private readonly List<string> _lines;

    /// <summary>   Gets the number of lines. </summary>
    /// <value> The number of lines. </value>
    public int LineCount => this._lines.Count;

    private bool _handleCreatedCheckEnabled;

    /// <summary>   Gets or sets a value indicating whether the <see cref="TextBoxTraceEventWriter"/> 
    /// checks if the handlers of the writer control, <see cref="ContainerTreeNode"/> or <see cref="ContainerControl"/>
    /// is created before updating their values. When Enabled, these controls update only after the container form is 
    /// fully loaded. As a result, the controls do not reflect any events that were logged when the form is created. </summary>
    /// <value> True if appending, false if not. </value>
    [Category( "Appearance" )]
    [Description( "True to check for control handles when updating the control values" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( false )]
    public bool HandleCreatedCheckEnabled
    {
        get => this._handleCreatedCheckEnabled;
        set => this.SetProperty( ref this._handleCreatedCheckEnabled, value );
    }

    private bool _appending;

    /// <summary>   Gets or sets a value indicating whether texts are appending or pre-pending. </summary>
    /// <value> True if appending, false if not. </value>
    [Category( "Appearance" )]
    [Description( "True to append; otherwise prepend" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( false )]
    public bool Appending
    {
        get => this._appending;
        set => this.SetProperty( ref this._appending, value );
    }

    private int _resetCount;

    /// <summary> Gets or sets the reset count. </summary>
    /// <remarks>
    /// The text box number of lines get reset to the preset count when the line count exceeds the reset
    /// count.
    /// </remarks>
    /// <value> <c>ResetSize</c>is an integer property. </value>
    [Category( "Appearance" )]
    [Description( "Number of lines at which to reset" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "100" )]
    public int ResetCount
    {
        get => this._resetCount;
        set => this.SetProperty( ref this._resetCount, value );
    }

    private int _presetCount;

    /// <summary> Gets or sets the preset count. </summary>
    /// <remarks>
    /// The text box number of lines get reset to the preset count when the line count exceeds the reset
    /// count.
    /// </remarks>
    /// <value> <c>PresetSize</c>is an integer property. </value>
    [Category( "Appearance" )]
    [Description( "Number of lines to reset to" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "50" )]
    public int PresetCount
    {
        get => this._presetCount;
        set => this.SetProperty( ref this._presetCount, value );
    }

    #endregion

    #region " visibility management "

    /// <summary> Query if the target is visible to the user. </summary>
    /// <value> The showing. </value>
    protected bool UserVisible => this._target.Visible && this._target.Height > 100
                    && this._target.GetContainerControl()?.ActiveControl is Control ctrl
                    && ctrl.Visible;

    #endregion

    #region " caption "

    /// <summary> Suspend updates and release indicator controls. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public virtual void SuspendUpdatesReleaseIndicators()
    {
        Thread.Sleep( 10 );
        this.ContainerPanel = null;
        this.ContainerTreeNode = null;
    }

    /// <summary>   Gets or sets the observed text length. </summary>
    /// <value> The observed text length. </value>
    public int ObservedTextLength { get; private set; }

    /// <summary>   Gets or sets the text length. </summary>
    /// <value> The length of the text. </value>
    public int TextLength { get; private set; }

    /// <summary> Queries if has unread messages. </summary>
    /// <value> The new messages available. </value>
    public bool HasUnreadMessages => this.TextLength > this.ObservedTextLength;

    /// <summary> The tab caption. </summary>
    private string _tabCaption = "Log";

    /// <summary> Gets or sets the tab caption. </summary>
    /// <value> The tab caption. </value>
    [Category( "Appearance" )]
    [Description( "Default title for the parent tab" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Log" )]
    public string TabCaption
    {
        get => this._tabCaption;
        set
        {
            if ( this.SetProperty( ref this._tabCaption, value ?? string.Empty ) )
            {
                this.UpdateCaption();
            }
        }
    }

    private string _captionFormat = "{0} =";

    /// <summary> Gets or sets the caption format indicating that messages were added. </summary>
    /// <value> The tab caption format. </value>
    [Category( "Appearance" )]
    [Description( "Formats the tab caption with number of new messages" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "{0} =" )]
    public string CaptionFormat
    {
        get => this._captionFormat;
        set
        {
            if ( this.SetProperty( ref this._captionFormat, value ?? string.Empty ) )
            {
                this.UpdateCaption();
            }
        }
    }

    private string _caption = string.Empty;

    /// <summary> Gets or sets the caption. </summary>
    /// <value> The caption. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public string Caption
    {
        get => this._caption;
        set
        {
            if ( this.SetProperty( ref this._caption, value ?? string.Empty ) )
            {
                if ( this.ContainerPanel is not null )
                    this.SafeTextSetter( this.ContainerPanel, this.Caption );

                if ( this.ContainerTreeNode is not null )
                    this.SafeTextSetter( this.ContainerTreeNode, this.Caption );
            }
        }
    }

    /// <summary> Updates the caption. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void UpdateCaption()
    {
        this.Caption = this.HasUnreadMessages
            ? string.Format( System.Globalization.CultureInfo.CurrentCulture, this.CaptionFormat, this.TabCaption )
            : this.TabCaption;
    }

    /// <summary> Gets or sets the container panel. </summary>
    /// <value> The parent panel. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public Panel? ContainerPanel { get; set; }

    /// <summary> Gets or sets the container tree node. </summary>
    /// <value> The container tree node. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TreeNode? ContainerTreeNode { get; set; }

    #endregion

    #region " safe setters "

    /// <summary> Updates the container panel caption. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="panel"> The container panel. </param>
    /// <param name="value"> The value. </param>
    private void SafeTextSetter( Panel panel, string value )
    {
        try
        {
            if ( panel.InvokeRequired )
            {
                _ = panel.BeginInvoke( new Action<Panel, string>( this.SafeTextSetter ), [panel, value] );
            }
            else if ( !this.HandleCreatedCheckEnabled || this._target.IsHandleCreated )
            {
                panel.Text = value;
            }
        }
        catch
        {
        }
    }

    /// <summary> Updates the container tree node caption. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="node">  The container tree node. </param>
    /// <param name="value"> The value. </param>
    private void SafeTextSetter( TreeNode node, string value )
    {
        try
        {
            if ( node.TreeView?.InvokeRequired == true )
            {
                _ = (node.TreeView?.BeginInvoke( new Action<TreeNode, string>( this.SafeTextSetter ), [node, value] ));
            }
            else if ( !this.HandleCreatedCheckEnabled || this._target.IsHandleCreated )
            {
                node.Text = value;
            }
        }
        catch
        {
        }
    }

    #endregion

    #region " target events "

    /// <summary>
    /// Raises the <see cref="Control.VisibleChanged" /> event. Updates the display.
    /// </summary>
    /// <remarks>   David, 2020-09-24. </remarks>
    /// <param name="sender">   Source of the event. </param>
    /// <param name="e">        An <see cref="EventArgs" /> that contains the event data. </param>
    private void HandleTargetVisibleChanged( object? sender, EventArgs e )
    {
        if ( this.UserVisible )
        {
            this.ObservedTextLength = this.TextLength;
        }
        this.UpdateCaption();
    }

    #endregion
}
