using System.Diagnostics;

namespace cc.isr.Tracing.WinForms;

/// <summary>   A text box writer trace listener. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
public class TextBoxWriterTraceListener : TraceListener, ITextWriter
{
    private readonly TextBox _target;
    private readonly StringSendDelegate _invokeWrite;

    /// <summary>   Constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="target">   Target for the. </param>
    public TextBoxWriterTraceListener( TextBox target )
    {
        this._target = target;
        this._invokeWrite = new StringSendDelegate( this.SendString );
    }

    /// <summary>
    /// When overridden in a derived class, writes the specified message to the listener you create
    /// in the derived class.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void Write( string? message )
    {
        _ = string.IsNullOrWhiteSpace( message )
            ? null
            : this._target.Invoke( this._invokeWrite, [message!] );
    }

    /// <summary>
    /// When overridden in a derived class, writes a message to the listener you create in the
    /// derived class, followed by a line terminator.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void WriteLine( string? message )
    {
        _ = string.IsNullOrWhiteSpace( message )
            ? null
            : this._target.Invoke( this._invokeWrite, [message + Environment.NewLine] );
    }

    /// <summary>   String send delegate. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  The message. </param>
    private delegate void StringSendDelegate( string message );

    /// <summary>   Sends a string. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    private void SendString( string message )
    {
        // No need to lock text box as this function will only 
        // ever be executed from the UI thread
        this._target.AppendText( message );
    }
}
