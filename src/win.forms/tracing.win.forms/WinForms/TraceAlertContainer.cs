using System.ComponentModel;
using System.Diagnostics;

namespace cc.isr.Tracing.WinForms;

/// <summary>   A base container for a trace event alert Annunciator. </summary>
/// <remarks>   David, 2021-03-05. </remarks>
public class TraceAlertContainer : ITraceEventWriter, INotifyPropertyChanged
{
    #region " construction "

    /// <summary>   (Immutable) the alert annunciator. </summary>
    private object? AlertAnnunciator { get; set; }

    private readonly Control _traceWriter;

    /// <summary>   Constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="alertAnnunciator"> The alert annunciator; supported are <see cref="Control"/> or <see cref="ToolStripItem"/>. </param>
    /// <param name="traceWriter">      The trace writer. </param>
    public TraceAlertContainer( object alertAnnunciator, Control traceWriter )
    {
        this._alertAnnunciatorEvent = TraceEventType.Verbose;
        this._alertSoundEvent = TraceEventType.Verbose;
        this.Name = System.Guid.NewGuid().ToString();
        this._traceWriter = traceWriter;
        this.AlertAnnunciator = alertAnnunciator;
        if ( this.AlertAnnunciator is Control control )
        {
            control.Click += this.OnClear;
        }
        else if ( this.AlertAnnunciator is ToolStripItem toolStripItem )
        {
            toolStripItem.Click += this.OnClear;
        }
        else
        {
            throw new InvalidOperationException( $"{alertAnnunciator?.GetType()} is not supported" );
        }
        this._traceWriter.VisibleChanged += new EventHandler( this.OnClear );
        if ( this._traceWriter.GetContainerControl()?.ActiveControl is Control ctrl )
            ctrl.VisibleChanged += new EventHandler( this.OnClear );
    }

    #endregion

    #region " notify property change implementation "

    /// <summary>   Occurs when a property value changes. </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <param name="propertyName"> Name of the property. </param>
    protected virtual void OnPropertyChanged( string? propertyName )
    {
        if ( !string.IsNullOrEmpty( propertyName ) )
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Executes the 'property changed' action. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="backingField"> [in,out] The backing field. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected virtual bool OnPropertyChanged<TValue>( ref TValue backingField, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = "" )
    {
        if ( EqualityComparer<TValue>.Default.Equals( backingField, value ) )
            return false;

        backingField = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="prop">         [in,out] The property. </param>
    /// <param name="value">        The value. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( ref TValue prop, TValue value, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
        if ( EqualityComparer<TValue>.Default.Equals( prop, value ) ) return false;
        prop = value;
        this.OnPropertyChanged( propertyName );
        return true;
    }

    /// <summary>   Sets a property. </summary>
    /// <remarks>   2023-03-24. </remarks>
    /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    ///                                             null. </exception>
    /// <typeparam name="TValue">    Generic type parameter. </typeparam>
    /// <param name="oldValue">     The old value. </param>
    /// <param name="newValue">     The new value. </param>
    /// <param name="callback">     The callback. </param>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    /// <returns>   <see langword="true"/> if it succeeds; otherwise, <see langword="false"/>. </returns>
    protected bool SetProperty<TValue>( TValue oldValue, TValue newValue, Action callback, [System.Runtime.CompilerServices.CallerMemberName] string? propertyName = null )
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull( callback, nameof( callback ) );
#else
        if ( callback is null ) throw new ArgumentNullException( nameof( callback ) );
#endif
#endif

        if ( EqualityComparer<TValue>.Default.Equals( oldValue, newValue ) )
        {
            return false;
        }

        callback();

        this.OnPropertyChanged( propertyName );

        return true;
    }

    /// <summary>   Notifies a property changed. </summary>
    /// <remarks>   2021-02-01. </remarks>
    /// <param name="propertyName"> (Optional) Name of the property. </param>
    protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName] string propertyName = "" )
    {
        this.PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
    }

    /// <summary>   Removes the property changed event handlers. </summary>
    /// <remarks>   2021-06-28. </remarks>
    protected void RemovePropertyChangedEventHandlers()
    {
        PropertyChangedEventHandler? handler = this.PropertyChanged;
        if ( handler is not null )
        {
            foreach ( Delegate? item in handler.GetInvocationList() )
            {
                handler -= ( PropertyChangedEventHandler ) item;
            }
        }
    }

    #endregion

    #region " i trace event writer implementation "

    /// <summary>   Gets or sets the name of the text writer. </summary>
    /// <value> The name. </value>
    public string Name { get; set; }

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    public TraceEventType TraceLevel { get; set; }

    /// <summary>
    /// Writes the specified trace event type to the contained annunciator.
    /// </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    public void TraceEvent( TraceEventType eventType, string message )
    {
        // the alert annunciator is only interested in the event type.
        this.TraceEvent( eventType );
    }

    #endregion

    #region " alert management "

    /// <summary> Suspend updates and release the Annunciator object. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public virtual void SuspendUpdatesReleaseAnnunciator()
    {
        Thread.Sleep( 10 );
        this.AlertAnnunciator = null;
    }

    /// <summary>   Writes the trace event type to the contained Annunciator. </summary>
    /// <remarks>   David, 2021-07-29. </remarks>
    /// <param name="eventType">    Type of the event. </param>
    public void TraceEvent( TraceEventType eventType )
    {
        try
        {
            this.AlertAnnunciatorEvent = eventType;
            this.AlertSoundEvent = eventType;
        }
        catch ( Exception ex )
        {
            this.OnEventHandlerError( ex );
        }
    }

    /// <summary>   Updates the alert annunciator as described by the alert event type. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="alertEvent">   The alert event. </param>
    private void UpdateAlertAnnunciator( TraceEventType alertEvent )
    {
        try
        {
            if ( this.AlertAnnunciator is Control control )
            {
                this.UpdateAlertAnnunciator( control, alertEvent );
            }
            else if ( this.AlertAnnunciator is ToolStripItem toolStripItem )
            {
                this.UpdateAlertAnnunciator( toolStripItem, alertEvent );
            }
        }
        catch ( Exception ex )
        {
            this.OnEventHandlerError( ex );
        }
    }

    /// <summary>   Updates the alert annunciator control as described by alertEvent. </summary>
    /// <remarks>   David, 2021-03-05. </remarks>
    /// <param name="control">      The alert annunciator control. </param>
    /// <param name="alertEvent">   The alert event. </param>
    private void UpdateAlertAnnunciator( Control control, TraceEventType alertEvent )
    {
        if ( control.InvokeRequired )
        {
            _ = control.BeginInvoke( new Action<Control, TraceEventType>( this.UpdateAlertAnnunciator ), [control, alertEvent] );
        }
        else if ( control.IsHandleCreated )
        {
            if ( alertEvent <= this.AlertLevel )
            {
                control.Text = alertEvent.ToString();
                switch ( alertEvent )
                {
                    case var @case when @case <= TraceEventType.Error:
                        {
                            control.BackColor = System.Drawing.Color.Red;
                            break;
                        }

                    case var case1 when case1 <= TraceEventType.Warning:
                        {
                            control.BackColor = System.Drawing.Color.Orange;
                            break;
                        }

                    default:
                        {
                            control.BackColor = System.Drawing.Color.LightBlue;
                            break;
                        }
                }

                if ( control is not TabPage )
                    control.Visible = true;
            }
            else
            {
                control.BackColor = this.AlertAnnunciatorBackColor;
                control.Text = this.AlertAnnunciatorText;
                if ( control is not TabPage )
                    control.Visible = false;
            }

            control.Invalidate();
        }
    }

    /// <summary>
    /// Updates the alert annunciator <see cref="ToolStripItem"/> as described by alertEvent.
    /// </summary>
    /// <remarks>   David, 2021-07-29. </remarks>
    /// <param name="toolStripItem">    The alert annunciator tool strip item. </param>
    /// <param name="alertEvent">       The alert event. </param>
    private void UpdateAlertAnnunciator( ToolStripItem toolStripItem, TraceEventType alertEvent )
    {
        if ( toolStripItem is not null && toolStripItem.GetCurrentParent() is not null )
        {
            if ( toolStripItem.GetCurrentParent()!.InvokeRequired )
            {
                _ = toolStripItem.GetCurrentParent()!.BeginInvoke( new Action<ToolStripItem, TraceEventType>( this.UpdateAlertAnnunciator ),
                                                                    [toolStripItem, alertEvent] );
            }
            else if ( toolStripItem.GetCurrentParent()!.Parent is Control ctrl && ctrl.IsHandleCreated )
            {
                if ( alertEvent <= this.AlertLevel )
                {
                    toolStripItem.Text = alertEvent.ToString();
                    switch ( alertEvent )
                    {
                        case var @case when @case <= TraceEventType.Error:
                            {
                                toolStripItem.BackColor = System.Drawing.Color.Red;
                                break;
                            }

                        case var case1 when case1 <= TraceEventType.Warning:
                            {
                                toolStripItem.BackColor = System.Drawing.Color.Orange;
                                break;
                            }

                        default:
                            {
                                toolStripItem.BackColor = System.Drawing.Color.LightBlue;
                                break;
                            }
                    }
                    toolStripItem.Visible = true;
                }
                else
                {
                    toolStripItem.BackColor = this.AlertAnnunciatorBackColor;
                    toolStripItem.Text = this.AlertAnnunciatorText;
                    toolStripItem.Visible = false;
                }

                toolStripItem.Invalidate();
            }
        }
    }

    #endregion

    #region " alert properties "

    private TraceEventType _alertLevel;

    /// <summary>
    /// Gets or sets the alert level. Message <see cref="TraceEventType">levels</see> equal or lower
    /// than this are tagged as alerts and set the <see cref="AlertAnnunciatorEvent">alert
    /// sentinel</see>.
    /// </summary>
    /// <value> The alert level. </value>
    [Category( "Appearance" )]
    [Description( "Level for notifying of alerts" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "TraceEventType.Warning" )]
    public TraceEventType AlertLevel
    {
        get => this._alertLevel;
        set => this.SetProperty( ref this._alertLevel, value );
    }

    /// <summary> Gets the sentinel indicating that a alert was announced. </summary>
    /// <value> true if an alert was announced; otherwise, false. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool AlertAnnounced => this.AlertAnnunciatorEvent <= this.AlertLevel;

    private TraceEventType _alertAnnunciatorEvent;

    /// <summary>
    /// Gets or sets or set the trace event type which the alert annunciator.
    /// </summary>
    /// <value> The alert annunciator event. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType AlertAnnunciatorEvent
    {
        get => this._alertAnnunciatorEvent;
        set
        {
            if ( this.SetProperty( ref this._alertAnnunciatorEvent, value ) )
            {
                this.UpdateAlertAnnunciator( value );
            }
        }
    }

    /// <summary> Gets the sentinel indicating that an alert sound was emitted. </summary>
    /// <value> true if an alert sound was emitted; otherwise, false. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public bool AlertVoiced => this.AlertSoundEvent <= this.AlertLevel;

    /// <summary> The alert sound event. </summary>
    private TraceEventType _alertSoundEvent;

    /// <summary> Gets or sets the highest alert event for alert sounds. </summary>
    /// <value> The highest alert event for alert sounds. </value>
    [Browsable( false )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
    public TraceEventType AlertSoundEvent
    {
        get => this._alertSoundEvent;
        set
        {
            if ( this.SetProperty( ref this._alertSoundEvent, value ) )
            {
                this.PlayAlertIf( this.AlertVoiced, value );
            }
        }
    }

    /// <summary> Gets or sets the alert sound enabled. </summary>
    /// <value> The alert sound enabled. </value>
    [Category( "Appearance" )]
    [Description( "Enables playing alert sounds" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( false )]
    public bool AlertSoundEnabled { get; set; }

    private Color _alertAnnunciatorBackColor = System.Drawing.SystemColors.Control;

    /// <summary> Gets or sets the background color of the alert annunciator. </summary>
    /// <value> The background color of the alert annunciator. </value>
    [Category( "Appearance" )]
    [Description( "Default annunciator background color" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( typeof( Color ), "0xF0F0F0" )]
    public Color AlertAnnunciatorBackColor
    {
        get => this._alertAnnunciatorBackColor;
        set => this.SetProperty( ref this._alertAnnunciatorBackColor, value );
    }

    private string _alertAnnunciatorText = "Trace";

    /// <summary> Gets or sets the alert annunciator text. </summary>
    /// <value> The alert annunciator text. </value>
    [Category( "Appearance" )]
    [Description( "Default annunciator text" )]
    [Browsable( true )]
    [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
    [DefaultValue( "Trace" )]
    public string AlertAnnunciatorText
    {
        get => this._alertAnnunciatorText;
        set => this.SetProperty( ref this._alertAnnunciatorText, value );
    }

    #endregion

    #region " show or hide alert "

    /// <summary> Play alert if. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="condition"> true to condition. </param>
    /// <param name="level">     The level. </param>
    public void PlayAlertIf( bool condition, TraceEventType level )
    {
        if ( condition && this.AlertSoundEnabled )
        {
            PlayAlert( level );
        }
    }

    /// <summary> Play alert. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="level"> The level. </param>
    public static void PlayAlert( TraceEventType level )
    {
        if ( level == TraceEventType.Critical )
        {
            System.Media.SystemSounds.Exclamation.Play();
        }
        else if ( level == TraceEventType.Error )
        {
            System.Media.SystemSounds.Exclamation.Play();
        }
        else if ( level == TraceEventType.Warning )
        {
            System.Media.SystemSounds.Asterisk.Play();
        }
    }

    /// <summary> Updates the alerts described by level. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    /// <param name="level"> The level. </param>
    protected void UpdateAlerts( TraceEventType level )
    {
        if ( level <= this.AlertLevel )
        {
            // Updates the cached alert level if the level represents a higher alert level.
            if ( level < this.AlertAnnunciatorEvent )
            {
                this.AlertAnnunciatorEvent = level;
            }

            if ( level < this.AlertSoundEvent )
            {
                this.AlertSoundEvent = level;
            }
        }
        // Me._alertsAdded = Not Me.UserVisible AndAlso (Me.AlertsAdded OrElse (level <= Me.AlertLevel))
        // 2016 moved to highest alert level: If Not Me.UserVisible AndAlso Me.AlertsAdded Then Me.PlayAlert(level)
    }

    /// <summary> Clears the alert annunciator if the trace writer is visible. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    public void Display()
    {
        this.OnClear( this.AlertAnnunciator, EventArgs.Empty );
    }

    /// <summary> Executes the clear action. </summary>
    /// <remarks> David, 2020-09-24. </remarks>
    private void OnClear( object? sender, EventArgs e )
    {
        if ( this.UserVisible )
        {
            this.AlertAnnunciatorEvent = TraceEventType.Verbose;
            this.AlertSoundEvent = TraceEventType.Verbose;
        }
    }

    #endregion

    #region " trace writer visibility manaement "

    /// <summary>   Queries if the trace writer is visible to the user. </summary>
    /// <value> True if the trace writer is visible to the user; otherwise false. </value>
    private bool UserVisible => this._traceWriter.Visible && this._traceWriter.Height > 10
                && this._traceWriter.GetContainerControl() is Control ctrl && ctrl.Visible;

    #endregion

    #region " exception event handler "

    /// <summary>   Event queue for all listeners interested in <see cref="Exception"/> events. </summary>
    public event EventHandler<ThreadExceptionEventArgs>? ExceptionEventHandler;

    /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
    /// <remarks>   David, 2021-05-03. </remarks>
    /// <param name="e">    Event information to send to registered event handlers. </param>
    protected virtual void OnEventHandlerError( ThreadExceptionEventArgs e )
    {
        this.ExceptionEventHandler?.Invoke( this, e );
    }

    /// <summary>   Raises the  <see cref="ExceptionEventHandler"/> event. </summary>
    /// <remarks>   David, 2021-07-29. </remarks>
    /// <param name="exception">    The exception. </param>
    protected virtual void OnEventHandlerError( Exception exception )
    {
        this.OnEventHandlerError( new ThreadExceptionEventArgs( exception ) );
    }

    #endregion
}
