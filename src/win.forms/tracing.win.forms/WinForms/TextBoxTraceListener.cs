using System.Diagnostics;

namespace cc.isr.Tracing.WinForms;

/// <summary>   Implements a <see cref="TraceListener"/> using a <see cref="TextBox"/> with <see cref="ITraceEventWriter"/>
/// <see cref="TraceLevel"/> filtering of the trace message. </summary>
/// <remarks>   David, 2021-02-08. </remarks>
public class TextBoxTraceListener : TraceListener, ITraceEventWriter
{
    private readonly TextBox _target;
    private readonly StringSendDelegate _invokeWrite;

    /// <summary>   Constructor. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="target">   Target for the. </param>
    public TextBoxTraceListener( TextBox target )
    {
        this._target = target;
        this._invokeWrite = new StringSendDelegate( this.SendString );
    }

    /// <summary>
    /// When overridden in a derived class, writes the specified message to the listener you create
    /// in the derived class.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void Write( string? message )
    {
        _ = string.IsNullOrWhiteSpace( message )
            ? null
            : this._target.Invoke( this._invokeWrite, [message!] );
    }

    /// <summary>
    /// When overridden in a derived class, writes a message to the listener you create in the
    /// derived class, followed by a line terminator.
    /// </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    public override void WriteLine( string? message )
    {
        _ = string.IsNullOrWhiteSpace( message )
            ? null
            : this._target.Invoke( this._invokeWrite, [message + Environment.NewLine] );
    }

    /// <summary>   String send delegate. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  The message. </param>
    private delegate void StringSendDelegate( string message );

    /// <summary>   Sends a string. </summary>
    /// <remarks>   David, 2021-02-08. </remarks>
    /// <param name="message">  A message to write. </param>
    private void SendString( string message )
    {
        // No need to lock text box as this function will only 
        // ever be executed from the UI thread
        this._target.AppendText( message );
    }

    /// <summary>   Gets or sets the trace level. </summary>
    /// <value> The trace level. </value>
    public TraceEventType TraceLevel { get; set; }

    /// <summary>   Trace event. </summary>
    /// <remarks>   David, 2021-03-06. </remarks>
    /// <param name="eventType">    Type of the event. </param>
    /// <param name="message">      The message. </param>
    public void TraceEvent( TraceEventType eventType, string message )
    {
        if ( eventType <= this.TraceLevel )
            this.WriteLine( message );
    }
}
