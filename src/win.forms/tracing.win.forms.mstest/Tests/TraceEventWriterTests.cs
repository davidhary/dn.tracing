using System.Diagnostics;

namespace cc.isr.Tracing.WinForms.Tests;

/// <summary>   (Unit Test Class) trace event writer tests. </summary>
/// <remarks>   David, 2020-09-23. </remarks>
[TestClass]
public class TraceEventWriterTests
{
    #region " construction and cleanup "

    /// <summary> Initializes the test class before running the first test. </summary>
    /// <remarks>
    /// Use <see cref="InitializeTestClass(TestContext)"/> to run code before running the first test in the class.
    /// </remarks>
    /// <param name="testContext"> Gets or sets the test context which provides information about
    /// and functionality for the current test run. </param>
    [ClassInitialize()]
    public static void InitializeTestClass( TestContext testContext )
    {
        try
        {
            string name = $"{testContext.FullyQualifiedTestClassName}.TraceListener";
            TraceListener = new TestWriterQueueTraceListener( name, SourceLevels.Warning );
            _ = Trace.Listeners.Add( TraceListener );
        }
        catch
        {
            // cleanup to meet strong guarantees
            try
            {
                CleanupTestClass();
            }
            finally
            {
            }

            throw;
        }
    }

    /// <summary> Cleans up the test class after all tests in the class have run. </summary>
    /// <remarks> Use <see cref="CleanupTestClass"/> to run code after all tests in the class have run. </remarks>
    [ClassCleanup( ClassCleanupBehavior.EndOfClass )]
    public static void CleanupTestClass()
    {
        Trace.Listeners.Remove( TraceListener );
        TraceListener?.Dispose();
    }

    /// <summary> Initializes the test class instance before each test runs. </summary>
    /// <remarks> David, 2020-09-18. </remarks>
    [TestInitialize()]
    public void InitializeBeforeEachTest()
    {
        Console.WriteLine( $"{this.TestContext?.FullyQualifiedTestClassName}: {DateTime.Now} {TimeZoneInfo.Local}" );
        TraceListener?.ClearQueue();
    }

    /// <summary> Cleans up the test class instance after each test has run. </summary>
    /// <remarks> David, 2020-09-23. </remarks>
    [TestCleanup()]
    public void CleanupAfterEachTest()
    {
        if ( TraceListener is not null && !TraceListener.Queue.IsEmpty )
            Assert.Fail( $"Errors or warnings were traced:{Environment.NewLine}{string.Join( Environment.NewLine, [.. TraceListener.Queue] )}" );
    }

    /// <summary>   Gets or sets the trace listener. </summary>
    /// <value> The trace listener. </value>
    private static TestWriterQueueTraceListener? TraceListener { get; set; }

    /// <summary>
    /// Gets or sets the test context which provides information about and functionality for the
    /// current test run.
    /// </summary>
    /// <value> The test context. </value>
    public TestContext? TestContext { get; set; }

    #endregion

    #region " text box text writer "

    private TextBoxTraceEventWriter? TextBoxTextWriter { get; set; }

    /// <summary>   (Unit Test Method) should trace message. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    private async Task AssertShouldTraceMessage()
    {
        // this is required to create the control handle.
        using Form panel = new();
        TextBox target = new()
        {
            WordWrap = false,
            Multiline = true,
            ReadOnly = true,
            CausesValidation = false,
            ScrollBars = ScrollBars.Both
        };
        panel.Controls.Add( target );
        // this is required to ensure the controls have handles for consummating the invocation;
        panel.Visible = true;
        bool handleCreated = target.IsHandleCreated;

        this.TextBoxTextWriter = new( target )
        {
            TabCaption = "Log",
            CaptionFormat = "{0} " + System.Text.Encoding.GetEncoding( 1200 ).GetString( [240] ),
            ResetCount = 1000,
            PresetCount = 500,
            TraceLevel = TraceEventType.Information
        };

        // the tracing listener is already registered.
        // _ = System.Diagnostics.Trace.Listeners.Add( Core.Tracing.TracingPlatform.Instance.TraceEventWriterTraceListener );

        // add the text box text writer.
        TracingPlatform.Instance.AddTraceEventWriter( this.TextBoxTextWriter );

        // this can be used for debugging
        // TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
        // Trace.TraceWarning( "testing a warning" );

        int initialTextLength = this.TextBoxTextWriter.TextLength;
        int initialLineCount = this.TextBoxTextWriter.LineCount;

        // send a trace message and see if it gets recorded. 
        string message = $"Message {DateTimeOffset.UtcNow}";

        Trace.TraceInformation( message );

        // Note: This was using Has Unread Messages; but with making the panel visible, 
        // Has Unread Messages could become false if Trace Writer finds the target text box visible.
        // This was an issue with the Messages Box Tests but not with this test. 
        // Still, the code here was change to determine success based on the actual message length.

#if false
        DateTime endTime = DateTime.Now.AddMilliseconds( 200 );
        while ( ( this.TextBoxTextWriter.TextLength <= initialTextLength) && DateTime.Now < endTime )
        {
            System.Threading.Tasks.Task.Delay( 1 ).Wait();

            // this is required otherwise the text Box Trace Event Writer invoke hangs.

            System.Windows.Forms.Application.DoEvents();
        }

        Assert.IsTrue( this.TextBoxTextWriter.TextLength > initialTextLength, "Text should be added to the messages box" );

#elif true
        Task<bool> awaitMessageTask = Task.Run( async () =>
        {
            while ( this.TextBoxTextWriter.LineCount <= initialLineCount )
                await Task.Delay( 10 );
            return this.TextBoxTextWriter.LineCount > initialLineCount;
        } );
        await awaitMessageTask;
        Assert.IsFalse( awaitMessageTask.IsCanceled, "Task cancelled" );
        Assert.IsTrue( awaitMessageTask.IsCompleted, "Task did not complete" );
#if NET5_0_OR_GREATER
        Assert.IsTrue( awaitMessageTask.IsCompletedSuccessfully, "Task did not complete successfully" );
#endif
        Assert.IsTrue( awaitMessageTask.Result, "Text should be added to the messages box" );
#endif

        int expectedLineCount = initialLineCount + 1;
        Assert.AreEqual( expectedLineCount, this.TextBoxTextWriter.LineCount,
            $"The number of messages added to this listener should match the expected value {target.Text}" );

    }

    /// <summary>   (Unit Test Method) should trace message. </summary>
    /// <remarks>   David, 2020-09-23. </remarks>
    [TestMethod()]
    public void ShouldTraceMessage()
    {
        _ = this.AssertShouldTraceMessage().Wait( 200 );
    }

    #endregion

    #region " trace alert container "

    private TraceAlertContainer? TraceAlertContainer { get; set; }

    /// <summary>   (Unit Test Method) should trace alert. </summary>
    /// <remarks>   David, 2021-07-29. </remarks>
    [TestMethod()]
    public void ShouldTraceAlert()
    {
        // this is required to create the control handle.
        using Form panel = new();
        using TextBox traceWriter = new();
        using Button alertAnnunciator = new();
        panel.Controls.Add( traceWriter );
        panel.Controls.Add( alertAnnunciator );
        // this is required to ensure the controls have handles for consummating the invocation;
        panel.Visible = true;
        bool handleCreated = traceWriter.IsHandleCreated;
        handleCreated = alertAnnunciator.IsHandleCreated;

        this.TraceAlertContainer = new( alertAnnunciator, traceWriter )
        {
            AlertAnnunciatorText = "Log",
            AlertLevel = TraceEventType.Information,
            AlertSoundEnabled = true
        };

        // the tracing listener is already registered.
        // _ = System.Diagnostics.Trace.Listeners.Add( Core.Tracing.TracingPlatform.Instance.TraceEventWriterTraceListener );

        // add the text box text writer.
        TracingPlatform.Instance.AddTraceEventWriter( this.TraceAlertContainer );

        // this can be used for debugging
        // TracingPlatform.Instance.AddTraceEventWriter( TracingPlatform.Instance.CriticalTraceEventTraceListener );
        // Trace.TraceWarning( "testing a warning" );

        // send a trace message and see if it gets recorded. 
        string message = $"Message {DateTimeOffset.UtcNow}";

        Trace.TraceWarning( message );

        DateTime endTime = DateTime.Now.AddMilliseconds( 20000 );
        while ( !(this.TraceAlertContainer.AlertAnnounced & this.TraceAlertContainer.AlertVoiced) && DateTime.Now < endTime )
            Task.Delay( 1 ).Wait();

        Assert.IsTrue( this.TraceAlertContainer.AlertAnnounced, "Alert should be announced" );
        Assert.IsTrue( this.TraceAlertContainer.AlertVoiced, "Alert should be voiced" );
        TraceListener?.ClearQueue();

    }

    #endregion
}
